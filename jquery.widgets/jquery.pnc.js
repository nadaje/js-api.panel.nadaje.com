// panel.nadaje.com jquery widget set
(function($) {
    var StreamingHistoryPreview = function(element, options) {
        if(this === window) {
            return new StreamingHistoryPreview(element, options);
        }
        this.init(element, options);
    };

    StreamingHistoryPreview.prototype = {
        BASE_API_URL: 'http://api.panel.nadaje.com/api/2.0/radio/',
        init: function(element, options) {
            var that = this;
            this.$element = $(element);
            var defaults = {
                // You can find api token under settings->streaming section in radio panel
                token: undefined,
                pageSize: 10,
                autorefresh: false,
                // remove jingles and ads from history
                skipJingles: true,
                skipAds: true,
                // point which radio stream history you want to fetch
                // ignore this option if your radio contains only one stream
                streamName: undefined
            };
            this.options = $.extend({}, defaults, options, this.$element.data());
            that.historyList = $('<ul id="' + that.options.streamName + '"></ul>');
            that.historyList.appendTo(this.$element);

            that._preview();
            if(this.options.autorefresh) {
                setInterval(function() {
                    that._preview();
                }, 20000);
            }
        },
        _preview: function() {
            var that = this;
            var previewUri =  this.BASE_API_URL + this.options.token + '/history/';
            var historyItem, streamHistoryPreview;
            var history = [];
            var loadHistoryPreview = function(historyPreview) {
                if(historyPreview.length === 0) {
                    throw {
                        message: 'Empty stream history list!'
                    };
                }
                if(historyPreview.length === 1 || that.options.streamName === undefined) {
                    streamHistoryPreview = historyPreview[0];
                } else {
                    streamHistoryPreview = $.grep(historyPreview, function(hp) {
                        return hp.stream_name === that.options.streamName;
                    })[0];
                    if(streamHistoryPreview === undefined) {
                        throw {
                            message: ('Incorrect stream name (pass one of following values:' +
                                      $.map(historyPreview, function(h) {return h.stream_name;}) + ')')
                        };
                    }
                }
                that._setupHistory(streamHistoryPreview);
            };
            if ( window.XDomainRequest ) {
                var xdr = new XDomainRequest();
                xdr.onload = function() {
                    loadHistoryPreview($.parseJSON(xdr.responseText));
                };
                xdr.open('get', previewUri);
                xdr.send();
            } else {
                $.getJSON(previewUri, loadHistoryPreview);
            }
        },
        _setupHistory: function(streamHistoryPreview) {
            var that = this, item;
            that.historyList.children().remove();
            var itemTemplate = $(
                '<li>' +
                    '<span class="artist"></span> - <span class="title"></span>' +
                 '</li>'
            );
            if(streamHistoryPreview.history_preview.length > 0) {
                for(i=0; i < streamHistoryPreview.history_preview.length && i < that.options.pageSize; i++) {
                    historyItem = streamHistoryPreview.history_preview[i];
                    if((!that.options.skipJingles || historyItem.track_type !== 'jingle') &&
                       (!that.options.skipAds || historyItem.track_type !== 'ad')) {
                        item = itemTemplate.clone();
                        $('.artist', item).text(historyItem.artist);
                        $('.title', item).text(historyItem.title);
                        that.historyList.append(item);
                    }
                }
            }
        }
    };
    $.fn.streamingHistoryPreview = function(options) {
        return this.each(function() {
            var $this = $(this);
            var data = $this.data('streaming-history-preview');
            if(!data) {
                $this.data('streaming-history-preview',
                           (data = new StreamingHistoryPreview(this, options)));
            }
            if (typeof options === 'string') {
                data[options]();
            }
        });
    };
})(window.jQuery);
